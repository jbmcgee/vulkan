#define VK_NO_PROTOTYPES

#include <vulkan/vulkan.h>
#include <assert.h>
#include <stdlib.h>

#define VK_EXPORTED_FUNCTIONS                               \
VK_EXPORTED_FUNCTION(vkGetInstanceProcAddr)

#define VK_GLOBAL_FUNCTIONS                                 \
VK_GLOBAL_FUNCTION(vkCreateInstance)                        \
VK_GLOBAL_FUNCTION(vkEnumerateInstanceExtensionProperties)  \
VK_GLOBAL_FUNCTION(vkEnumerateInstanceLayerProperties)       

#define VK_INSTANCE_FUNCTIONS                               \
VK_INSTANCE_FUNCTION(vkDestroyInstance)                     \
VK_INSTANCE_FUNCTION(vkEnumeratePhysicalDevices)            \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceProperties)         \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceFeatures)           \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceQueueFamilyProperties)  \
VK_INSTANCE_FUNCTION(vkCreateDevice)                        \
VK_INSTANCE_FUNCTION(vkGetDeviceProcAddr)                   \
VK_INSTANCE_FUNCTION(vkEnumerateDeviceExtensionProperties)

#define VK_EXPORTED_FUNCTION(n, ...) PFN_##n n;
VK_EXPORTED_FUNCTIONS
#undef VK_EXPORTED_FUNCTION

#define VK_GLOBAL_FUNCTION(n, ...) PFN_##n n;
VK_GLOBAL_FUNCTIONS
#undef VK_GLOBAL_FUNCTION

#define VK_INSTANCE_FUNCTION(n, ...) PFN_##n n;
VK_INSTANCE_FUNCTIONS
#undef VK_INSTANCE_FUNCTION

#ifdef _WIN32
#define VK_USE_PLATFORM_MACOS_MVK
#define GetLibrary LoadLibrary
#define GetAddress GetProcAddress
#define Library HMODULE
#define VkLibraryName "vulkan-1.dll"
#else
#include <dlfcn.h>
#define GetLibrary(n) dlopen(n, RTLD_NOW)
#define GetAddress dlsym
#define Library void*
#define VkLibraryName "libvulkan.1.dylib"
#endif

#define MAX_LAYER_PROPERTIES 16
#define MAX_DEVICES 8

VkInstance          m_instance;
VkLayerProperties*  m_layerProperties[MAX_LAYER_PROPERTIES];
VkDevice*           m_devices[MAX_DEVICES];

void renderer_init()
{
    Library libVulkan = GetLibrary(VkLibraryName);
    assert(libVulkan != NULL);

#define  VK_EXPORTED_FUNCTION(n, ...) if( !(n = (PFN_##n)GetAddress( libVulkan, #n )) ) assert(NULL);
    VK_EXPORTED_FUNCTIONS
#undef  VK_EXPORTED_FUNCTION

#define VK_GLOBAL_FUNCTION(n, ...) if( !(n = (PFN_##n)vkGetInstanceProcAddr( NULL, #n )) ) assert(NULL);
    VK_GLOBAL_FUNCTIONS
#undef VK_GLOBAL_FUNCTION

    uint32_t layerCount =  sizeof(m_layerProperties) / sizeof(VkLayerProperties);
    vkEnumerateInstanceLayerProperties(&layerCount, m_layerProperties);

    VkApplicationInfo appInfo = { 0 };
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "VulkanDemo";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "VGX";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo = { 0 };
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    assert(vkCreateInstance(&createInfo, NULL, &m_instance) == VK_SUCCESS);

#define VK_GLOBAL_FUNCTION(n, ...) if( !(n = (PFN_##n)vkGetInstanceProcAddr( m_instance, #n )) ) assert(NULL);
    VK_GLOBAL_FUNCTIONS
#undef VK_GLOBAL_FUNCTION

    uint32_t numDevices = sizeof(numDevices) / sizeof(VkPhysicalDevice);
    assert(vkEnumeratePhysicalDevices( m_instance, &numDevices, NULL ) == VK_SUCCESS);

    VkPhysicalDevice* physicalDevices = (VkPhysicalDevice*)calloc(numDevices, sizeof(VkPhysicalDevice));
    assert(vkEnumeratePhysicalDevices( m_instance, &numDevices, &physicalDevices[0] ) == VK_SUCCESS );


}

void renderer_uninit()
{
}