#include <stdio.h>

extern void renderer_init();

int main(int argc, char* argv[])
{
    renderer_init();
    return 0;
}